<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\ResponseResolver;

use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

interface ResponseResolverInterface
{
    /**
     * @param Request $request
     * @param NodeInterface $node
     *
     * @return bool
     */
    public function supports(Request $request, NodeInterface $node): bool;

    /**
     * @param Request $request
     * @param NodeInterface $node
     *
     * @return Response
     *
     * @throws HttpExceptionInterface
     */
    public function getResponse(Request $request, NodeInterface $node): Response;
}
