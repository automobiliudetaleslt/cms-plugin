<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Manager;

use Doctrine\ORM\EntityRepository;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Omni\Sylius\CmsPlugin\Model\RelatableInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

class RelationManager
{
    /**
     * @var RepositoryInterface[]
     */
    private $repositories = [];

    /**
     * @var NodeTypeManager
     */
    private $nodeTypeManager;

    /**
     * @param NodeTypeManager $nodeTypeManager
     */
    public function __construct(NodeTypeManager $nodeTypeManager)
    {
        $this->nodeTypeManager = $nodeTypeManager;
    }

    /**
     * @param string $resourceId
     * @param RepositoryInterface $repository
     */
    public function addRepository(string $resourceId, RepositoryInterface $repository)
    {
        $this->repositories[$resourceId] = $repository;
    }

    /**
     * @param string $resourceId
     *
     * @return EntityRepository
     */
    public function getRepository(string $resourceId): ?EntityRepository
    {
        return $this->repositories[$resourceId] ?? null;
    }

    /**
     * @param RelatableInterface $relatable
     *
     * @return ResourceInterface|null
     */
    public function getRelation(RelatableInterface $relatable): ?ResourceInterface
    {
        // So that if the method is called for the same resource >1 times there would not be any excess DB calls
        if ($relatable->getRelation()) {
            return $relatable->getRelation();
        }

        if (null === $relatable->getRelationType() || null === $relatable->getRelationId()) {
            return null;
        }

        if (false === isset($this->repositories[$relatable->getRelationType()])) {
            throw new \InvalidArgumentException('The resource ' . $relatable->getRelationType() . ' is not configured!');
        }

        $relation = $this->repositories[$relatable->getRelationType()]->find($relatable->getRelationId());

        if (null === $relation) {
            return null;
        }

        $relatable->setRelation($relation);

        return $relation;
    }

    /**
     * @param NodeInterface $node
     */
    public function instantiateRelations(NodeInterface $node): void
    {
        $relations = [];

        foreach ($this->getRelationDataFromSubNodes($node) as $resourceType => $relationIds) {
            if (!isset($this->repositories[$resourceType])) {
                continue;
            }

            /** @var ResourceInterface $relation */
            foreach ($this->repositories[$resourceType]->findBy(['id' => $relationIds]) as $relation) {
                $relations[$resourceType][$relation->getId()] = $relation;
            }
        }

        $this->setNodeRelations($node, $relations);
    }

    /**
     * @param TranslatableInterface $relation
     *
     * @return string
     *
     * @throws \Exception
     */
    public function getRelationLabel(TranslatableInterface $relation)
    {
        $translation = $relation->getTranslation();

        if (method_exists($translation, 'getName')) {
            return $translation->getName();
        }

        if (method_exists($translation, 'getTitle')) {
            return $translation->getTitle();
        }

        throw new \Exception(
            'The class ' . get_class($translation) . ' must have `getName` or `getTitle` method'
        );
    }

    /**
     * Returns  [ 'resouce_type_1' => [ 1, 5, 63, 82, ... ], 'resource_type_2' => [ ... ] ]
     *
     * Where the numbers are ids
     *
     * @param NodeInterface $node
     *
     * @return array
     */
    protected function getRelationDataFromSubNodes(NodeInterface $node): array
    {
        $response = [];

        foreach ($node->getChildren() as $subnode) {
            if ($subnode->getRelationType() && $subnode->getRelationId()) {
                $response[$subnode->getRelationType()][] = $subnode->getRelationId();

                $response = array_merge_recursive($response, $this->getRelationDataFromSubNodes($subnode));
            }
        }

        return $response;
    }

    /**
     * @param NodeInterface $node
     *
     * @param array $relations
     */
    protected function setNodeRelations(NodeInterface $node, array $relations)
    {
        if ($node->getRelationType() && $node->getRelationId()) {
            $relation = isset($relations[$node->getRelationType()][$node->getRelationId()]) ?
                $relations[$node->getRelationType()][$node->getRelationId()] : null;

            $node->setRelation($relation);
        }

        foreach ($node->getChildren() as $subNode) {
            $this->setNodeRelations($subNode, $relations);
        }
    }
}
