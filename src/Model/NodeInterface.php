<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Omni\Sylius\SeoPlugin\Model\SeoAwareInterface;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\ImagesAwareInterface;
use Sylius\Component\Resource\Model\CodeAwareInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableInterface;
use Sylius\Component\Resource\Model\ToggleableInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;

/**
 * @method NodeTranslationInterface getTranslation(?string $locale = null)
 */
interface NodeInterface extends
    CodeAwareInterface,
    ResourceInterface,
    TimestampableInterface,
    ToggleableInterface,
    TranslatableInterface,
    ImagesAwareInterface,
    SeoAwareInterface,
    RelatableInterface
{
    /**
     * @return string
     */
    public function getType(): ?string;

    /**
     * @param string $type
     *
     * @return NodeInterface
     */
    public function setType(?string $type): NodeInterface;

    /**
     * @return null|string
     */
    public function getName(): ?string;

    /**
     * @return int
     */
    public function getLeft(): ?int;

    /**
     * @param int $left
     *
     * @return NodeInterface
     */
    public function setLeft(?int $left): NodeInterface;

    /**
     * @return int
     */
    public function getLevel(): ?int;

    /**
     * @param int $level
     *
     * @return NodeInterface
     */
    public function setLevel(?int $level): NodeInterface;

    /**
     * @return int
     */
    public function getRight(): ?int;

    /**
     * @param int $right
     *
     * @return NodeInterface
     */
    public function setRight(?int $right): NodeInterface;

    /**
     * @return NodeInterface
     */
    public function getRoot(): ?NodeInterface;

    /**
     * @param NodeInterface $root
     *
     * @return NodeInterface
     */
    public function setRoot(NodeInterface $root): NodeInterface;

    /**
     * @return NodeInterface
     */
    public function getParent(): ?NodeInterface;

    /**
     * @param NodeInterface $parent
     *
     * @return NodeInterface
     */
    public function setParent(?NodeInterface $parent): NodeInterface;

    /**
     * @return bool
     */
    public function hasChildren(): bool;

    /**
     * @return ArrayCollection|NodeInterface[]
     */
    public function getChildren(): Collection;

    /**
     * @param Collection|ArrayCollection|NodeInterface[] $children
     *
     * @return NodeInterface
     */
    public function setChildren(Collection $children): NodeInterface;

    /**
     * @param NodeInterface $child
     *
     * @return NodeInterface
     */
    public function addChild(NodeInterface $child): NodeInterface;

    /**
     * @param NodeInterface $child
     *
     * @return NodeInterface
     */
    public function removeChild(NodeInterface $child): NodeInterface;

    /**
     * @return string
     */
    public function getLink(): ?string;

    /**
     * @return Collection|ChannelInterface[]
     */
    public function getChannels(): Collection;

    /**
     * @param ChannelInterface $chanels
     *
     * @return Node
     */
    public function removeChannel(ChannelInterface $channel): NodeInterface;

    /**
     * @param ChannelInterface $chanels
     *
     * @return Node
     */
    public function addChannel(ChannelInterface $channel): NodeInterface;

    /**
     * @return bool
     */
    public function isSlugFromRelation(): bool;

    /**
     * @param bool $slugFromRelation
     *
     * @return $this
     */
    public function setSlugFromRelation(bool $slugFromRelation);

    /**
     * @return string|null
     */
    public function getSlug(): ?string;

    /**
     * @return string|null
     */
    public function getLinkTarget(): ?string;

    /**
     * @return string|null
     */
    public function getTitle(): ?string;

    public function hasEnabledChildren(): bool;
}
