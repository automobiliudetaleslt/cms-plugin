<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Model;

use Omni\Sylius\SeoPlugin\Model\SeoAwareTranslationInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\SlugAwareInterface;
use Sylius\Component\Resource\Model\TranslationInterface;

/**
 * @method NodeInterface getTranslatable()
 */
interface NodeTranslationInterface extends
    ResourceInterface,
    TranslationInterface,
    SeoAwareTranslationInterface,
    SlugAwareInterface
{
    /**
     * @return string
     */
    public function getTitle(): ?string;

    /**
     * @param string $title
     *
     * @return NodeTranslationInterface
     */
    public function setTitle(?string $title);

    /**
     * @return string
     */
    public function getContent(): ?string;

    /**
     * @param string $content
     *
     * @return NodeTranslationInterface
     */
    public function setContent(?string $content);

    /**
     * @return string|null
     */
    public function getLink(): ?string;

    /**
     * @param string|null $link
     *
     * @return NodeTranslationInterface
     */
    public function setLink(?string $link);

    /**
     *
     * @return string|null
     */
    public function getLinkTarget(): ?string;

    /**
     * @param string|null $linkTarget
     *
     * @return NodeTranslationInterface
     */
    public function setLinkTarget(?string $linkTarget);
}
