<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Model;

use Sylius\Component\Core\Model\Image;
use Sylius\Component\Resource\Model\TranslatableTrait;

class NodeImage extends Image implements NodeImageInterface
{
    use RelatableTrait;
    use TranslatableTrait {
        __construct as private initializeTranslationsCollection;
    }

    public function __construct()
    {
        $this->initializeTranslationsCollection();
    }

    /**
     * @var string
     */
    protected $position;

    /**
     * {@inheritdoc}
     */
    public function getPosition(): ?string
    {
        return $this->position;
    }

    /**
     * {@inheritdoc}
     */
    public function setPosition(?string $position): NodeImageInterface
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return bool
     */
    public function isLinkable(): bool
    {
        return (bool) $this->relationId || (bool) $this->getTranslation()->getLink();
    }

    /**
     * {@inheritdoc}
     */
    protected function createTranslation()
    {
        return new NodeImageTranslation();
    }
}
