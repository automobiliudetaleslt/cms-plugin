<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Form\Type;

use Omni\Sylius\CmsPlugin\Form\Subscriber\NodeImageSubscriber;
use Sylius\Bundle\CoreBundle\Form\Type\ImageType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NodeImageType extends ImageType
{
    /**
     * @var NodeImageSubscriber
     */
    protected $addRelationToImageSubscriber;

    /**
     * @param string                       $dataClass FQCN
     * @param string[]                     $validationGroups
     * @param NodeImageSubscriber $addRelationToImageSubscriber
     */
    public function __construct(
        string $dataClass,
        array $validationGroups = [],
        NodeImageSubscriber $addRelationToImageSubscriber
    ) {
        parent::__construct($dataClass, $validationGroups);

        $this->addRelationToImageSubscriber = $addRelationToImageSubscriber;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $addRelationToImageSubscriber = clone $this->addRelationToImageSubscriber;
        $addRelationToImageSubscriber->setNode($options['parent_data']);

        $builder->addEventSubscriber($addRelationToImageSubscriber);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => $this->dataClass,
            'validation_groups' => $this->validationGroups,
            'parent_data' => null,
        ]);
    }
}
