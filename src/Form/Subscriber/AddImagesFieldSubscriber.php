<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Form\Subscriber;

use Omni\Sylius\CmsPlugin\Form\Type\NodeImageType;
use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AddImagesFieldSubscriber implements EventSubscriberInterface
{
    /**
     * @var NodeTypeManager
     */
    private $handler;

    /**
     * @param NodeTypeManager $handler
     */
    public function __construct(NodeTypeManager $handler)
    {
        $this->handler = $handler;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();

        if ($data instanceof NodeInterface && $this->handler->isImagesAware($data->getType())) {
            $event->getForm()->add('images', CollectionType::class, [
                'entry_type' => NodeImageType::class,
                'allow_add' => true,
                'required' => false,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => 'sylius.form.product.images',
                'entry_options' => ['parent_data' => $data],
            ]);
        }
    }
}
