<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Form\Subscriber;

use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Model\NodeTranslationInterface;
use Omni\Sylius\SeoPlugin\Form\Type\SeoMetadataType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class SeoMetadataSubscriber implements EventSubscriberInterface
{
    /**
     * @var NodeTypeManager
     */
    private $nodeTypeManager;

    /**
     * @param NodeTypeManager $nodeTypeManager
     */
    public function __construct(NodeTypeManager $nodeTypeManager)
    {
        $this->nodeTypeManager = $nodeTypeManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        /** @var NodeTranslationInterface $translation */
        if (null === $translation = $event->getData()) {
            return;
        }

        if ($this->nodeTypeManager->isSeoAware($translation->getTranslatable()->getType())) {
            $event->getForm()->add('seoMetadata', SeoMetadataType::class, [
                'label' => 'omni_sylius.form.node.metadata',
                'required' => false,
            ]);
        }
    }
}
