<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Menu;

use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class ContentMenuBuilder
{
    /**
     * @param MenuBuilderEvent $event
     */
    public function configureNodeMenu(MenuBuilderEvent $event)
    {
        $contentMenu = $this->getContentMenu($event);

        $contentMenu
            ->addChild('node', ['route' => 'omni_sylius_admin_node_index'])
            ->setLabel('omni_sylius.menu.admin.node')
            ->setLabelAttribute('icon', 'folder open outline')
        ;
    }

    /**
     * @param MenuBuilderEvent $event
     *
     * @return ItemInterface
     */
    private function getContentMenu(MenuBuilderEvent $event)
    {
        $adminMenu = $event->getMenu();
        $contentMenu = $adminMenu->getChild('content');

        if (null === $contentMenu) {
            $contentMenu = $adminMenu
                ->addChild('content')
                ->setLabel('omni_sylius.menu.admin.header')
            ;
        }

        return $contentMenu;
    }
}
