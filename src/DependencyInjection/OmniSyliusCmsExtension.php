<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\DependencyInjection;

use Sylius\Bundle\ResourceBundle\DependencyInjection\Extension\AbstractResourceExtension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

class OmniSyliusCmsExtension extends AbstractResourceExtension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $config, ContainerBuilder $container)
    {
        $config = $this->processConfiguration($this->getConfiguration($config, $container), $config);
        $container->setParameter('omni_cms.image_upload_dir', $config['image_upload_dir']);
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
        $this->registerResources('omni_sylius', $config['driver'], $config['resources'], $container);

        $nodeTypeManagerDefinition = $container->getDefinition('omni_sylius.manager.node_type');
        $layoutableNodes = [];

        foreach ($config['node_types'] as $nodeType => $options) {
            $nodeTypeManagerDefinition->addMethodCall('addType', [$nodeType, $this->getNodeOptions($options, $container)]);
            if ($options['layout']) {
                $layoutableNodes[] = $nodeType;
            }
        }

        $container->setParameter('omni_sylius_cms.layoutable.nodes', $layoutableNodes);
    }

    /**
     * {@inheritdoc}
     */
    public function prepend(ContainerBuilder $container)
    {
        // TODO: Implement prepend() method.
    }

    /**
     * @param array            $options
     * @param ContainerBuilder $container
     *
     * @return array
     */
    private function getNodeOptions(array $options, ContainerBuilder $container): array
    {
        if (isset($options['relation'])) {
            $options['relation'] = $this->getRelationOptions($options['relation'], $container);
        }

        if (isset($options['images']['relations'])) {
            $relations = $options['images']['relations'];
            $options['images']['relations'] = [];

            foreach ($relations as $relation) {
                $options['images']['relations'][] = $this->getRelationOptions($relation, $container);
            }
        }

        foreach ($options['images']['available_positions'] as $key => $position) {
            $options['images']['available_positions'][$position] = $position;
            unset($options['images']['available_positions'][$key]);
        }

        return $options;
    }

    /**
     * @param string           $resource
     * @param ContainerBuilder $container
     *
     * @return array
     */
    private function getRelationOptions(string $resource, ContainerBuilder $container): array
    {
        $options = [];
        $this->addRelationRepository($resource, $container);
        $options['alias'] = $resource;
        $options['class'] = $container->getParameter(
            sprintf('%s.model.%s.class', ...explode('.', $resource))
        );
        $options['name'] = substr($options['class'], strrpos($options['class'], '\\') + 1);;

        return $options;
    }

    /**
     * @param string           $resource
     * @param ContainerBuilder $container
     */
    private function addRelationRepository(string $resource, ContainerBuilder $container): void
    {
        $container->getDefinition('omni_sylius.manager.relation')->addMethodCall(
            'addRepository',
            [
                $resource,
                new Reference(sprintf('%s.repository.%s', ...explode('.', $resource)))
            ]
        );
    }
}
