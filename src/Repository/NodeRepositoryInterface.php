<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Repository;

use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

/**
 * @method NodeInterface|null findOneByType(string $type)
 * @method NodeInterface|null findOneByCode(string $type)
 */
interface NodeRepositoryInterface extends RepositoryInterface
{
    /**
     * @return NodeInterface[]
     */
    public function findRootNodes();

    /**
     * @param string $slug
     *
     * @return mixed
     */
    public function findOneBySlug($slug);

    /**
     * @param string $type
     * @param int $levels
     *
     * @return NodeInterface|null
     */
    public function findOneWithChildrenByType(string $type, int $levels = 1): ?NodeInterface;

    /**
     * @param string $type
     * @param int $levels
     *
     * @return NodeInterface|null
     */
    public function findOneWithChildrenByCode(string $type, int $levels = 1): ?NodeInterface;
}
